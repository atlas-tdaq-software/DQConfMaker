"""
Test DQOksConfMaker Module
andrea.dotti@pi.infn.it
15 August 2008
"""
from __future__ import print_function
from DQOksConfMaker.okswriter import OksDocument,writeOksConfiguration
import DQConfMakerBase

def testme():
    import DQConfMakerBase.DQElements
    algo  = DQConfMakerBase.DQElements.DQAlgorithm( 'gaus_fit' , libname="lib.so" )
    param = DQConfMakerBase.DQElements.DQParameter( id='aParameter',algorithm=algo,inputdatasource="testhisto")
    writer=OksDocument()
    writer.addObject(algo)
    writer.addObject(param)
    writer.finalize()
    print(writer.toprettyxml('  '))
    print('Writing configuration to file')
    writeOksConfiguration(configList=[algo,param])
if __name__=="__main__":
    testme()
