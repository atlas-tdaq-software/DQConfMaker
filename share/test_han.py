"""
Test DQHanConfMaker Module
author: andrea.dotti@pi.infn.it
        ponyisi@hep.uchicago.edu
17 August 2008
"""
from builtins import range
from DQConfMakerBase.DQElements import *
from DQConfMakerBase.Helpers import IDHelper, make_thresholds
from DQHanConfMaker.hanwriter import writeHanConfiguration

def testme():
    checkmean = DQAlgorithm(id='AlgCheckMean', 
                            libname='libDataQualityInterfacesAlgs.so')
    
    gatherdata = DQAlgorithm(id='GatherData',
                             libname='libDataQualityInterfacesAlgs.so')
    
    compalg = CompositeAlgorithm(id='AlgCheckMean&GatherData', subalgorithms=[checkmean, gatherdata])
    useablecompalg = DQAlgorithm(id='AlgCheckMean&GatherData', libname='', compalg=compalg)

    reference = DQReference(reference='Reference.root:same_name')
    checkmeanthresh = make_thresholds(name='chi2', warning=2, error=3)
    
    worst = DQAlgorithm(id='WorstCaseSummary',libname='libdqm_summaries.so')
    indiv = DQRegion(id='Individual',algorithm=worst )
    summary = DQRegion(id='Summary',algorithm=worst,dqregions=indiv )

    for name in ('nthMean', 'nthMeanPull', 'meanPull', 'nthRMS'):
#        summary.newDQParameter(id=name, algorithm=checkmean, inputdatasource='eb1/Stats/Gaussian/' + name, 
#                               references=reference, thresholds=checkmeanthresh)
         summary.newDQParameter(id=name, algorithm=useablecompalg, inputdatasource='eb1/Stats/Gaussian/' + name, 
                               references=reference, thresholds=checkmeanthresh)
                   
    for i in range(100):
        hist = indiv.newDQParameter(id='gaus_%d' % i, algorithm=checkmean, inputdatasource='eb1/Stats/Gaussian/gaus_%d' % i, 
                                    references=reference, thresholds=checkmeanthresh)
        hist.addAnnotation('display', 'LogY')
       
    writeHanConfiguration( filename = 'test.config' , roots = summary )
    
if __name__=="__main__":
    testme()
