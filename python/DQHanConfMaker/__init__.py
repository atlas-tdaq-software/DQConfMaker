## @package DQHanConfMaker
# This package is used to create DQMF configuration files for the offline framework.
# The output format is han files.
# @version: 1
# @author: andrea.dotti@pi.infn.it
# @author: ponyisi@hep.uchicago.edu
# @requires: DQConfMakerBase
"""
This is the package to write han configuration files for DQMF.
It transforms DQElements in the corresponding han format.
This package have been inspired by the xml.minidom package.
"""

## Enumerate the elements
from future import standard_library
standard_library.install_aliases()
from builtins import object
class Node(object):
    """
    Enumeration of the possible han elements
    """
    UNKNOWN     = -1
    DOCUMENT    = 0
    DIR         = 1
    HIST        = 2
    ALGORITHM   = 3
    THRESHOLD   = 4
    REFERENCE   = 5
    OUTPUT      = 6
    LIMIT       = 7
    COMPALG     = 8

## @return: string representation for han elements 
def _get_NodeType( n ):
    """
    This function returns the han keyword for each element
    """
    if n == Node.DIR: return 'dir'
    elif n == Node.ALGORITHM: return 'algorithm'
    elif n == Node.HIST: return 'hist'
    elif n == Node.OUTPUT: return 'output'
    elif n == Node.REFERENCE: return 'reference'
    elif n == Node.THRESHOLD: return 'thresholds'
    elif n == Node.DOCUMENT: return ''
    elif n == Node.LIMIT: return 'limits'
    elif n == Node.COMPALG: return 'compositeAlgorithm'
    else: return 'unknown'
    
def _get_StringIO():
    import io, six
    return io.BytesIO() if six.PY2 else io.StringIO()

"""list of dq configuration elements for which a valid han representation exists"""
_acceptedHanElements=['DQParameter','DQThreshold','DQReference','DQRegion','DQAlgorithm',
                      'DQAlgorithmParameter','CompositeAlgorithm']

## @return: True if han supports the specified configuration element
# @param object: the object to check
def _isAceptedHanElement( object ):
    """
    Check if dq configuration element has a valid han representation
    """
    if object.type in _acceptedHanElements:
        return True
    return False

""" Default output attribute for histogram element """
_default_output = 'top_level'
