## The header of OKS XML files

"""
DocType string for OKS XML files
"""
documenttype='oks-schema [ \n \
  <!ELEMENT oks-schema (info, (include)?, (comments)?, (class)+)> \n \
  <!ELEMENT info EMPTY> \n \
  <!ATTLIST info\n \
      name CDATA #REQUIRED\n \
      type CDATA #REQUIRED\n \
      num-of-includes CDATA #REQUIRED\n \
      num-of-items CDATA #REQUIRED\n \
      oks-format CDATA #FIXED "schema"\n \
      oks-version CDATA #REQUIRED\n \
      created-by CDATA #REQUIRED\n \
      created-on CDATA #REQUIRED\n \
      creation-time CDATA #REQUIRED\n \
      last-modified-by CDATA #REQUIRED\n \
      last-modified-on CDATA #REQUIRED\n \
      last-modification-time CDATA #REQUIRED\n \
  >\n \
  <!ELEMENT include (file)+>\n \
  <!ELEMENT file EMPTY>\n \
  <!ATTLIST file\n \
      path CDATA #REQUIRED\n \
  >\n \
  <!ELEMENT comments (comment)+>\n \
  <!ELEMENT comment EMPTY>\n \
  <!ATTLIST comment\n \
      creation-time CDATA #REQUIRED\n \
      created-by CDATA #REQUIRED\n \
      created-on CDATA #REQUIRED\n \
      author CDATA #REQUIRED\n \
      text CDATA #REQUIRED\n \
  >\n \
  <!ELEMENT class (superclass | attribute | relationship | method)*>\n \
  <!ATTLIST class\n \
      name CDATA #REQUIRED\n \
      description CDATA ""\n \
      is-abstract (yes|no) "no"\n \
  >\n \
  <!ELEMENT superclass EMPTY>\n \
  <!ATTLIST superclass name CDATA #REQUIRED>\n \
  <!ELEMENT attribute EMPTY>\n \
  <!ATTLIST attribute\n \
      name CDATA #REQUIRED\n \
      description CDATA ""\n \
      type (bool|s8|u8|s16|u16|s32|u32|s64|u64|float|double|date|time|string|uid|enum|class) #REQUIRED\n \
      range CDATA ""\n \
      format (dec|hex|oct) "dec"\n \
      is-multi-value (yes|no) "no"\n \
      multi-value-implementation (list|vector) "list"\n \
      init-value CDATA ""\n \
      is-not-null (yes|no) "no"\n \
  >\n \
  <!ELEMENT relationship EMPTY>\n \
  <!ATTLIST relationship\n \
      name CDATA #REQUIRED\n \
      description CDATA ""\n \
      class-type CDATA #REQUIRED\n \
      low-cc (zero|one) #REQUIRED\n \
      high-cc (one|many) #REQUIRED\n \
      is-composite (yes|no) #REQUIRED\n \
      is-exclusive (yes|no) #REQUIRED\n \
      is-dependent (yes|no) #REQUIRED\n \
      multi-value-implementation (list|vector) "list"\n \
  >\n \
  <!ELEMENT method (method-implementation*)>\n \
  <!ATTLIST method\n \
      name CDATA #REQUIRED\n \
      description CDATA ""\n \
  >\n \
  <!ELEMENT method-implementation EMPTY>\n \
  <!ATTLIST method-implementation\n \
      language CDATA #REQUIRED\n \
      prototype CDATA #REQUIRED\n \
      body CDATA ""\n \
  >\n \
  ]'