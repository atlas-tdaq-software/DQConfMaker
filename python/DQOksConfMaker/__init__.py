## @package DQOksConfMaker
# This package is used to create DQMF configuration files for the online framework
# The output format is OKS XML files
# @version: 1
# @author: andrea.dotti@pi.infn.it
# @author: ponyisi@hep.uchicago.edu
# @requires: DQConfMakerBase
"""
This is the package to write oks configuration files for DQMF
It transforms DQElements in the corresponding oks format.
"""

##list of dq configuration elements for which a valid OKS representation exists
_acceptedOksElements = [ 'DQAlgorithmParameter' , 'DQThreshold' , 'DQRunType' , 'DQDetectorMask','DQLuminosity','DQAlgorithm','DQReference','DQParameter','DQRegion']

## Check is the elemnt can be transformed in OKS format
# @param object: a DQElement
# @return: True if the object can be represented through a valid OKS element
# @note: This function is used internally and should not be used by users of this module
def _isAcceptedOksElement( object ):
    """
    Check if dq configuration element has a valid  OKS representation
    """
    if object.type in _acceptedOksElements:
        return True
    return False
    