## @package DQConfMakerBase.DQElements 
# Module containing all DQ elements that can be configured in a DQ job
# @version: 1
# @author: andrea.dotti@pi.infn.it
# @since: 13 Aug 2008
"""
This module contains the data quality elements.
All elements of DQ can be represented with one of these classes.
"""
from __future__ import absolute_import

from builtins import str
from builtins import object
from .Helpers import BaseException,IDHelper,toList,get_id_from_helper

## Base Exception
# Base class for all exceptions
class DQBaseError(BaseException):
    """
    Generic Error in creating DQ configuration element
    """
    ## Default constructor
    #@param msg: A human readable message
    def __init__(self,msg):
        BaseException.__init__(self,'Error in DQBase element:'+str(msg) )

## Exception thrown when adding a relation        
class DQBaseAddRelationError(BaseException):
    """
    Trying to add a new relation failed
    """
    ## Default constructor
    #@param relation: The relation name @type relation: a string
    #@param reasong: The explanation   
    def __init__(self, relation , reason):
        BaseException.__init__(self, 'Could not add relation '+relation+' Because '+reason)

## Exception thrown when removing a relation
class DQBaseDelRelationError(BaseException):
    """
    Trying to remove a relation failed
    """
    ## Default constructor
    #@param relation: The relation name @type relation: a string
    #@param reasong: The explanation   
    def __init__(self, relation , reason):
        BaseException.__init__(self, 'Could not remove relation '+relation+' Because '+reason)

## Exception thrown when object is of wrong type
class DQBaseNotValidObjectError(BaseException):
    """
    The object is not of the expected type
    """
    ## Default constructor
    #@param objName: The object name @type objName: a string
    #@param type: The type   
    def __init__(self,objName,type):
        BaseException.__init__(self,'Object '+objName+' is of wrong type: '+type)

## Exception thrown when a requested annotation is now found
class DQBaseNoSuchAnnotationError(BaseException):
    """
    The requested annotation is not known
    """
    ## Default constructor
    #@param annotation: The annotation name
    def __init__(self,annotation):
        BaseException.__init__(self,'Annotation '+annotation+' not present')

## Base Class for all DQ elements
# All elements that can be configured should be represented as a class inheriting from DQBase
class DQBase(object):
    """
    This Class specify a DQ configuration element.
    A DQ configuration element contains a list of attributes and relations
    to other DQ configuration elements.
    DQBase elements can have a directional relationship.
    i.e. a DQBase element can have a relation with one or more DQBase elements, forming a 
    direct graph.
    Attributes:
        id : the unique id of the DQ configuration element
        type : the type of the DQ configuration element (string)
        attr : the dictionary of the attributes of the form { 'name' : ( type , value) }
                where type indicates the data type (string) and value the actual value
        rels : the dictionary of the relations of the form { 'name' : [ DQBase ] }
        isReferencedBy : list of DQBase elements that relation this one
    """
    ## Default constructor
    # @param id: The element identifier
    # @param type: The type name of the derived element
    def __init__(self, id , type='DQBase'):
        """
        constructs a DQBase element with id and type
        """
        self.id=id
        self.type=type
        # The attributes dictionary
        # An attribute has a name (the key) and a tuple: ( type , value)
        # both type and value being strings.
        # For example:   { 'intAttr' : ( 'int' , 3 ) }
        self.attr = { }
        # The relations dictionary
        # The DQ configuration element can be linked to other DQ configuration elements
        # A relation is identified by a name (the key) and a list of DQBase elements
        self.rels = { }
        # This list contains the DQBase objects that set a relation to this.
        # This can be used to back navigate a DQBase relation tree
        self.isReferencedBy = [ ]
        # Depending on the output format, annotations may be supported
        self.annotations = { }
    ## Gets an attribute
    # @param name: the attribute name
    # @return: the attribute value
    def getAttribute(self, name ):
        """
        Returns the attribute identified by name
        """
        return self.attr[name]
    ## Adds an attribute
    # @param name: the attribute name
    # @param value: the value of the attribute
    # @param type: the type of the attribute ('int','float', etc)
    # @raise DQBaseAddRelationError: in case the relation already exists
    def addAttr(self , name , value , type='string'):
        """
        Adds an attribute identified by name with value of a type
        """
        if name in self.attr:
            raise DQBaseAddRelationError(name, 'Relation already exists')
        self.attr[name]=(type,value)
    ## Gets all attributes
    # @return: a dictionary containing all attributes
    def getAllAttributes(self):
        """
        Returns a dictionary containing all attributes
        """
        return self.attr.copy()
    ## Gets a relation
    # @param name: the relation name 
    # @return: the associated object
    def getRelation(self, name):
        """
        Returns the relation identified by name
        """
        if name in self.rels:
            return self.rels[name]
        else:
            return None
    ## Gets all relations
    # @return: a list of all objects associated to this
    def getAllRelatedObjects(self):
        """
        Returns all relations
        """
        rv = []
        for x in list(self.rels.values()):
            if x == [None]:
                continue
            rv += x
        return rv
    ## Adds a relation
    # @param name: the relation name
    # @param value: the object
    # @note: if the relation name already exists add value to the list of associated objects 
    def addRelation(self , name , value ):
        """
        Adds a relation object (value) to the relations identified by name 
        """
        if value:
            try:
                # Add self to the list of fathers of the child
                value.isReferencedBy += [ self ]
            except AttributeError:
                raise DQBaseAddRelationError( name , 'Object '+str(value)+' is not a DQBase object')    
        if name in self.rels:
            self.rels[name] += [ value ]
        else:
            self.rels[name] = [value]
    ## Deletes a relation
    # @param name: the relation name
    # @param value: the relation object
    # @raise DQBaseDelRelationError: if the relation is unknown
    def delRelation(self , name , value ):
        """
        Deletes the relation value with name
        """
        if value:
            try:
                # Add self to the list of fathers of the child
                if self in value.isReferencedBy:
                    value.isReferencedBy.remove(self)
            except AttributeError:
                raise DQBaseDelRelationError( name , 'Object '+str(value)+' is not a DQBase object')    
        if name in self.rels and value in self.rels[name]:
            self.rels[name].remove(value)
        else:
            raise DQBaseDelRelationError( name , 'Object '+str(value)+' does not already have relationship '+name)    
    ## Adds an annotation
    # @param name: the annotation name
    # @param text: the message of the annotations
    def addAnnotation(self, name, text):
        """
        Adds an annotation
        """
        self.annotations[name] = text
    ## Gets an annotation
    # @param name: the name of the annotation
    # @return: the value of the annotation      
    def getAnnotation(self, name):
        """
        Gets an annotation by name
        """
        try:
            return self.annotations[name]
        except AttributeError:
            raise DQBaseNoSuchAnnotationError(name)
    ## Gets all annotations
    # @return: all annotations
    def getAllAnnotations(self):
        """
        Returns all associated annotations
        """
        return self.annotations
    ## Resets the object
    def clear(self):
        """
        Removes all attributes, annotations and resets id and type
        """
        self.attr.clear()
        self.rels.clear()
        self.id = None
        self.type = No
    ## Converts to string
    def __str__(self):
        msg='id='+str(self.id)+' type='+str(self.type)+' Attr:'+str(self.attr)+' Rels:{'
        for k,v in self.rels.items():
            msg = msg + str(k) + ': ['
            for e in v:
                if e:
                    try:
                        msg= msg+str(e.id)+','
                    except:
                        raise DQBase.addAttr(self,'GreenThresholdsNames',toList(warningsnames),'string')                        
            msg += '],'
        msg = msg+'} IsReferencedBy:['
        for f in self.isReferencedBy:
            if f:
                try:
                    msg = msg+f.id+','
                except:
                    raise DQBase.addAttr(self,'GreenThresholdsNames',toList(warningsnames),'string')
        return msg + ']'
    def __repr__(self):
        return 'DQBase('+repr(self.id)+','+repr(self.type)+')'

## An algorithm parameter
class DQAlgorithmParameter(DQBase):
    """
    DQAlgorithnParameter configuration class
    Describes parameters that can be used by a DQAlgorithm
    """
    ## Default constructor
    # @param id: the configuration id
    #    @param name: the name for the DQAlgorithmParameter
    #    @param value: the value (or list of values) for the DQAlgorithmParameter
    #    @param type: the type of the value
    def __init__(self ,  name, value, id=IDHelper.getInstance(), type='double'):
        """
        Creates an DQAlgorithmParameter, i.e. a parameter used to configure an algorithm
        """
        myid = get_id_from_helper(id, self)
        DQBase.__init__(self,myid,type=self.__class__.__name__)
        DQBase.addAttr(self,'Name',name,'string')
        DQBase.addAttr(self,'Value',toList(value),type)
    def __repr__(self):
        val=self.getAttribute('Value')
        return self.__class__.__name__+'(id='+repr(self.id)+',name='+repr(self.getAttribute('Name')[1])+',value='+repr(val[1])+',type='+repr(val[0])+')'    
    # @return: name of algorithm parameter
    def getName(self):
        """
        get name of algorithm parameter
        """
        return self.getAttribute('Name')
    
    # @return: value of algorithm parameter
    def getValue(self):
        """
        get value of algorithm parameter
        """
        return self.getAttribute('Value')
    
## A Threshold
class DQThreshold(DQBase):
    """
    The configuration for a DQThreshold: red / green  (or warning/error)
    """
    ## Default constructor
    # @param id: the configuration id
    # @param name: the name of threshold
    # @param value:  the value for the DQThreshold
    # @param type: the type of the value
    def __init__(self, name , value, id=IDHelper.getInstance(), type='double'):
        """
        Creates a DQThreshold with name and value
        """
        myid = get_id_from_helper(id, self)
        DQBase.__init__(self,myid,type=self.__class__.__name__)
        DQBase.addAttr(self,'Name',name,'string')
        DQBase.addAttr(self,'Value', value,type)
    def __repr__(self):
        val=self.getAttribute('Value')
        return self.__class__.__name__+'(id='+repr(self.id)+',name='+repr(self.getAttribute('Name')[1])+',value='+repr(val[1])+',type='+repr(val[0])+')'

## A condition object base class
class DQCondition(DQBase):
    """
    A condition associated to a reference object.
    Describes the condition to be used with DQReference.
    This class is defined as virtual in schema,
    use DEtectorMask, RunType or Luminosity instead
    @todo: Support for non string value types
    """
    def __init__(self,value,id=IDHelper.getInstance(),conditionname='DQCondition',type="string"):
        myid = get_id_from_helper(id, self)
        DQBase.__init__(self,myid,type=conditionname)
        DQBase.addAttr(self,'Source',value,type)
    def __repr__(self):
        return self.__class__.__name__+'(id='+repr(self.id)+',value='+repr(self.getAttribute('Source')[1])+',conditionname='+repr(self.type)+',type='+repr(self.getAttribute('Source')[0])+')'

## A RunType condition
class DQRunType(DQCondition):
    """
    Concrete Condition: run type selector
    """
    ## Default constructor
    # @param value: the value of the RunType condition
    def __init__(self,value,id=IDHelper.getInstance()):
        """
        Creates a condition specifying the run type
        """
        DQCondition.__init__(self,str(value),id,conditionname='DQRunType')
    def __repr__(self):
        return self.__class__.__name__+'(id='+repr(self.id)+',value='+repr(self.getAttribute('Source')[1])+')'

## A detector mask condition
class DQDetectorMask(DQCondition):
    """
    Concrete Condition: detector mask selector
    """
    def __init__(self,value,id=IDHelper.getInstance()):
        """
        Creates a condition specifying the detector mask
        """
        DQCondition.__init__(self,str(value),id,conditionname='DQDetectorMask')
    def __repr__(self):
        return self.__class__.__name__+'(id='+repr(self.id)+',value='+repr(self.getAttribute('Source')[1])+')'

## A luminosity condition
class DQLuminosity(DQCondition):
    """
    Concrete Condition: luminosity selector
    """
    def __init__(self,value,id=IDHelper.getInstance()):
        """
        Creates a condition specifying luminosity
        """
        DQCondition.__init__(self,str(value),id,conditionname='Luminosity')
    def __repr__(self):
        return self.__class__.__name__+'(id='+repr(self.id)+',value='+repr(self.getAttribute('Source')[1])+')'

## A DQMF algorithm
class DQAlgorithm(DQBase):
    """
    DQAlgorithm configuration
    Describes the algorithm used to analyse the histogram
    """
    ## Default constructor
    #@param id: the algorithm name
    #@param parametersnames: the names of the needed DQAlgorithmParameters. 
    #@param warningssnames: the names of the DQThresholds for warning level.
    #@param errorsnames: the names of the DQThresholds for error level
    #@param compalg: a CompositeAlgorithm
    #@attention: : parametersnames, warningsnames and errorsnames are not the actual corresponding DQ objects.
    def __init__(self,id,libname='libdqm_algorithms.so',
                 parametersnames=None ,
                 warningsnames=None ,
                 errorsnames=None,
                 compalg=None):
        """
        Creates a dqmf algorithm configuration object.
        A DQAlgorithms represents a dqmf algorithm that can be configured through parameters and has thresholds.
        """
        DQBase.__init__(self,id,type=self.__class__.__name__)
        DQBase.addAttr(self,'LibraryName',libname,'string')
        if parametersnames:
            DQBase.addAttr(self,'ParametersNames',toList(parametersnames),'string')
        else:
            DQBase.addAttr(self,'ParametersNames',[],'string')
        if errorsnames:
            DQBase.addAttr(self,'RedThresholdsNames',toList(errorsnames),'string')
        else:
            DQBase.addAttr(self,'RedThresholdsNames',[],'string')
        if warningsnames:
            DQBase.addAttr(self,'GreenThresholdsNames',toList(warningsnames),'string')
        else:
            DQBase.addAttr(self,'GreenThresholdsNames',[],'string')
        if compalg:
            if not isinstance(compalg, CompositeAlgorithm):
                raise DQBaseNotValidObjectError(subalg, 'not a CompositeAlgorithm')
            self.addRelation('CompositeAlgorithm', compalg)
    ## @return: shared library name containing the algorithm code
    def getLibraryName(self):
        """
        The algorithm code is in this library
        """
        return self.getAttribute('LibraryName')[1]
    ## @return: The CompositeAlgorithm object that this algorithm is bases on. (None if no such object)
    def getCompositeAlgorithm(self):
        """
        The algorithm can be obtained as a CompositeAlgorithm. Note that composite algorithms exist and are valid only in the Han offline implementation of dqmf
        """
        rv = self.getRelation('CompositeAlgorithm')
        if rv == None:
            return rv
        else:
            return rv[0]
    # @todo: Add CompositeAlgorithm    
    def __repr__(self):
        return self.__class__.__name__+'(id='+self.id+',libname='+repr(self.getAttribute('LibraryName')[1])+',parametersname='+repr(self.getAttribute('ParametersNames')[1])+',greenthresholdsnames='+repr(self.getAttribute('GreenThresholdsNames')[1])+',redthresholdsnames='+repr(self.getAttribute('RedThresholdsNames')[1])+')'
            
## A reference for a test.
class DQReference(DQBase):
    """
    Describes the reference configuration. 
    The reference histogram corresponding
    to a certain condition (example: run type)
    """
    ## Default constructor
    #@param id: the configuration id
    #@param reference: the reference histogram
    #@param conditions: the DQConditions objects that apply for this reference @see: DQCondition
    def __init__(self, reference , id=IDHelper.getInstance(), conditions=None):
        """
        Creates a DQReference.
        The reference can be associated to one or more DQConditions objects that specify when the reference should be used
        """
        myid = get_id_from_helper(id, self)
        DQBase.__init__(self, myid , type=self.__class__.__name__)
        DQBase.addAttr( self, 'Reference', reference , 'string')
        if conditions:
            for e in toList(conditions):
                DQBase.addRelation(self, 'Conditions' , e)
    ## @return: the reference histogram name
    def getReference(self):
        return self.getAttribute('Reference')[1]    
    def __repr__(self):
        return self.__class__.__name__+'(id='+self.id+',reference='+repr(self.getAttribute('Reference')[1])+',conditions='+repr(self.getRelation('Conditions'))+')'

## A DQ Parameter configuration object
# @todo: In online DQMF there is the concept of "action" not yet supported, to be implemented
class DQParameter(DQBase):
    """
    A DQParameter is the basic element of a DQ job.
    It is the combination of input histogram(s), a concrete algorithm contained in a dq library and its configuration: algorithm parameters, thresholds, reference histogram (if needed).
    DQParameters can be organized in a  tree structure with the use of DQRegions objects.
    """
    ## Default constructor
    #@param id: the configuration id
    #@param algorithm: the DQAlgorithm object
    #@param inputdatasource: the input to be used. It is the full path name
    #@param inputdatatype: the type of the input Plain or RegExp
    #@param weight: the weight for the DQParameter. Used by SummaryMakers, if needed
    #@param action: the action to be taken in case of errors. NOT YET IMPLEMENTED
    #@param algorithmparameters: the DQAlgorithmPArameter(s) needed to configure the DQParameter
    #@param references: the DQReferences used by the algorithm, if needed 
    #@param warnings: the DQThresholds for the warning level (aka online: RedThresholds)
    #@param errors: the DQThresholds for the error level (aka online: GreenThresholds)
    #@param annotations: Additional information provided to the DQParameter    
    def __init__(self,id,algorithm=None,inputdatasource=None,inputdatatype='Plain',weight=1,action='',
                 algorithmparameters=None,references=None,warnings=None,errors=None,thresholds=None,
                 annotations = {}):
        """
        Creates a DQParameter specifying the DQAlgorithm object, the input histogram name and type ( one of 'Plain' or 'RegExp'), note in offline HAN DQMF RegExp are not yet supported as input.
        The weight can be used by the algorithm associated to the DQRegion this DQParameter belongs to. Depending on the algorithm some configuration parameters can be provided. Usually one or more threshold should be provided for both
        warning and error level
        """
        myid = get_id_from_helper(id, self)
        DQBase.__init__(self,myid,self.__class__.__name__)
        DQBase.addAttr(self, 'InputDataSource' , inputdatasource , type='string')
        DQBase.addAttr(self,'InputDataType',inputdatatype,type='enum')
        DQBase.addAttr(self,'Weight',weight,type='float')
        DQBase.addAttr(self,'Action',action, type='string')
        DQBase.addRelation(self, 'Algorithm' , algorithm)
        for ap in toList(algorithmparameters):
            DQBase.addRelation(self, 'AlgorithmParameters', ap)
        for r in toList(references):
            DQBase.addRelation(self, 'References', r)
        if thresholds != None:
            for thresh in toList(thresholds):
                self.addRelation('GreenThresholds', thresh['green'])
                self.addRelation('RedThresholds', thresh['red'])
        else:
            for g in toList(warnings):
                DQBase.addRelation(self, 'GreenThresholds', g)
            #if errors:
            for r in toList(errors):
                DQBase.addRelation(self, 'RedThresholds', r)
    def __repr__(self):
          msg=self.__class__.__name__+'(id='+repr(self.id)+',algorithm='+repr(self.getRelation('Algorithm')[0])+',inputdatasource='+repr(self.getAttribute('InputDataSource')[1])
          msg=msg+',inputdatatype='+repr(self.getAttribute('InputDataType')[1])+',weight='+repr(self.getAttribute('Weight')[1])+',action='+repr(self.getAttribute('Action')[1])
          msg=msg+',algorithmparameters='+repr(self.getRelation('AlgorithmParameters'))+',references='+repr(self.getRelation('References'))+',warnings='+repr(self.getRelation('GreenThresholds'))
          msg=msg+',errors='+repr(self.getRelation('RedThresholds'))
          return msg  
    ## @return: the input histogram
    def getInput(self):
        return self.getAttribute('InputDataSource')[1]
    ## @return: The associated DQAlgorithm object
    def getAlgorithm(self):
        return self.getRelation('Algorithm')[0]
    ## @return: The associated DQAlgorithmParamters
    def getAlgorithmParameters(self):
        return self.getRelation('AlgorithmParameters')
    ## @return: The associated (if any) DQReference object
    def getReference(self):
        return self.getRelation('References')
    ## @return: The list of DQThrehold for the warning level
    def getWarnings(self):
        return self.getRelation('GreenThresholds')
    ## @return: The list of DQThrehold for the error level
    def getErrors(self):
        return self.getRelation('RedThresholds')
    ## Helper to create a thresholds pair for the warning and error level
    #@param name: the threshold name
    #@param warning: the value for warning level
    #@param error: the value for error level
    def newThreshold(self, name, warning, error, id=IDHelper.getInstance()):
        """
        Algorithms usually use thresholds with the same name for warning/error level. This method allows to create easily two new thresholds with given values.
        """
        thresh = make_thresholds(name, warning, error, id)
        self.addRelation('GreenThresholds', thresh['green'])
        self.addRelation('RedThresholds', thresh['red'])

## A DQ Region configuration object.   
#@todo: Correct handling of action @see DQParameter
class DQRegion(DQParameter):
    """
    Region configuration
    Describes a group of DQPrameters or DQRegions belonging to the same logic unit. The status of the region depends on the status of children.
    """
    ## Default constructor
    #@param id: the region name
    #@param algorithm: the DQAlgorithm object
    #@param dqparameters: the list of DQParameters belonging to this DQRegion
    #@param dqregions: the list of DQRegions belonging to this DQRegion
    #@param inputdatasource: the input to be used. It is the full path name
    #@param inputdatatype: the type of the input Plain or RegExp
    #@param weight: the weight for the DQParameter. Used by SummaryMakers, if needed
    #@param action: the action to be taken in case of errors. NOT YET IMPLEMENTED
    #@param algorithmparameters: the DQAlgorithmPArameter(s) needed to configure the DQParameter
    #@param references: the DQReferences used by the algorithm, if needed 
    #@param warnings: the DQThresholds for the warning level (aka online: GreenThresholds)
    #@param errors: the DQThresholds for the error level (aka online: RedThresholds)
    def __init__(self,id,algorithm,dqparameters=None,dqregions=None,inputdatasource='',inputdatatype='Plain',weight=1,action='',
                 algorithmparameters=None,reference=None,warnings=None,errors=None):
        """
        Creates a DQRegion as a combination of one or more DQParameters and/or DQRegions.
        A DQRegion is similar to a DQParameter in the sense that it uses an algorithm to combine the results of the leaves, DQRegions or DQParameters in a new result.
        Thus all options available for a DQParameter are also available for a DQRegion.
        """
        myid = get_id_from_helper(id, self)
        DQParameter.__init__(self,myid,algorithm,inputdatasource,inputdatatype,weight,action,
                             algorithmparameters,reference,warnings,errors)
        if dqparameters != None:
            for p in toList(dqparameters):
                self.addRelation('DQParameters', p)
        if dqregions != None:
            for r in toList(dqregions):
                self.addRelation('DQRegions', r)
    def __repr__(self):
        msg=self.__class__.__name__+'(id='+repr(self.id)+',algorithm='+repr(self.getRelation('Algorithm')[0])+',dqparameters='+repr(self.getRelation('DQParameters'))+',dqregions='+repr(self.getRelation('DQRegions'))
        msg=msg+',inputdatasource='+repr(self.getAttribute('InputDataSource')[1])
        msg=msg+',inputdatatype='+repr(self.getAttribute('InputDataType')[1])+',weight='+repr(self.getAttribute('Weight')[1])+',action='+repr(self.getAttribute('Action')[1])
        msg=msg+',algorithmparameters='+repr(self.getRelation('AlgorithmParameters'))+',references='+repr(self.getRelation('References'))+',warnings='+repr(self.getRelation('GreenThresholds'))
        msg=msg+',errors='+repr(self.getRelation('RedThresholds'))
        return msg
    ## @return: the list of DQRegions associated to this DQRegion
    def getSubRegions(self):
        """
        The associated DQRegion are returned as a list, in case empty.
        """
        rv = self.getRelation('DQRegions')
        if rv == None:
            return []
        else:
            return rv
    ## @return: the list of DQParameters associated to this DQRegion 
    def getDQParameters(self):
        """
        The associated DQRegion are returned as a list, in case empty.
        """
        rv = self.getRelation('DQParameters')
        if rv == None:
            return []
        else:
            return rv
    ## Adds a new DQRegion
    # @param region: the DQRegion to add
    def addDQRegion(self, region):
        """
        A new DQRegion can be added to this region
        """
        self.addRelation('DQRegions', region)
    ## Adds a new DQParameter
    # @param param: the DQParameter to add
    def addDQParameter(self, param):
        """
        A new DQRegion can be added to this region
        """
        self.addRelation('DQParameters', param)
    ## Creates and adds a new DQParmaeter specifying the parameters 
    def newDQParameter(self, *args, **kw):
        """
        Creates new DQParameter in this DQRegion.  Arguments are passed to DQParameter constructor.
        """
        dqparam = DQParameter(*args, **kw)
        self.addDQParameter(dqparam)
        return dqparam
    ## Creates and adds a new DQRegion specifying the parameters
    def newDQRegion(self, *args, **kw):
        """
        Creates new DQRegion in this DQRegion. Arguments are passed to DQRegion constructor.
        """
        dqparam = DQRegion(*args, **kw)
        self.addDQRegion(dqparam)
        return dqparam

## A composite algorithm.
class CompositeAlgorithm(DQBase):
    """
    Represents a han composite algorithm
    """
    ## Default constructor
    #@param id: CompositeAlgorithm name
    #@param subalgorithms: list of subalgorithms.  Must be of type DQAlgorithm!
    def __init__(self, id, subalgorithms=[]):
        """
        Creates a new CompositeAlgorithm specifying the list of sub-DQAlgorithms
        Note that CompositeAlgorithms are specific of offline HAN implementation of DQMF
        """
        myid = get_id_from_helper(id, self)
        DQBase.__init__(self,myid,self.__class__.__name__)
        self.subalgorithms = []
        for subalg in subalgorithms:
            if not isinstance(subalg, DQAlgorithm):
                raise DQBaseNotValidObjectError(subalg, 'not a DQAlgorithm')
            self.addRelation('DQAlgorithms', subalg)
            
    def _getSubAlgs(self):
        subalgs = self.getRelation('DQAlgorithms')
        if subalgs == None:
            return []
        else:
            return subalgs
    
    ## @return: A list of names of sub algorithms    
    def getSubAlgNames(self):
        return [x.id for x in self._getSubAlgs()]

    ##@return: names of the necessary libraries as a list of strings.  There may be fewer libraries than subalgorithms.
    def getLibNames(self):
        return list(set([x.getLibraryName() for x in self._getSubAlgs() if x.getLibraryName() != '']))
