## @package DQConfMakerBase.algorithms
# A module containing all known DQMF algorithms.

"""
algorithms module
This module contains DQAlgorithm objects for all current algorithms contained in the official
dqm-common package.
"""
## @todo: Create configuration elements for all known dqm algorithms
