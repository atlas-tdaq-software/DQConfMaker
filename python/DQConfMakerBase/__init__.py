## @package DQConfMakerBase 
# Base package for all DQ Configuration packages
# This package contains basic elements and utilities to create DQ configurations for the online and offline versions of DQMF
# @version: 1
# @author: andrea.dotti@pi.infn.it
# @since: 13 Aug 2008
"""
This is the base package for creating DQMF configurations files for the online and offline frameworks
"""