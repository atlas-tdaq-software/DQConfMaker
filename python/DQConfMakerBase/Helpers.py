## @package DQConfMakerBase.Helpers
# Module containing some utilities and helpers
#@author: andrea.dotti@cern.ch
#@author: ponyisi@hep.uchicago.edu
#@since: 13 August 2008
"""
Helpers Module.
This module contains functions and classes used by DQConfMaker packages
"""
from __future__ import absolute_import

## Creates a list
#@param value: the input object
#@return: a list 
from builtins import str
from builtins import object
def toList(value):
    """
    Converts the argument to a list.
    """
    if type(value)==type([]):
        return value
    else:
        return [value]

## Generates a unique ID
# Ids are used to uniquely identify DQMF objects in configuration
#@param obj: A DQ object 
#@return: An ID
def get_id_from_helper(id, obj):
    """
    Generates a unique ID for DQ object obj.
    Obj must inherit from DQBase
    """
    if isinstance(id, IDHelper):
        return id.ID(obj.__class__.__name__)
    else:   
        return id

## ID Generation helpers
class IDHelper(object):
    """
    This class generates unique IDs for the DQ configuration objects - is a pseudo-singleton
    """
    # some standard prefixes
    thrTag="Thr"
    algParTag="AlgPar"
    dqParamTag="Par"
    dqRefTag="Ref"
    dqRegTag="Reg"
    dqCondTag="Cond"
    
    __singletons = {}
    
    ## Singleton getter
    # @see: constructor
    @classmethod
    def getInstance(cls,base=0,extratag=''):
        retval = cls.__singletons.get((base,extratag), None)
        if retval == None:
            retval = cls(base, extratag)
            cls.__singletons[(base,extratag)] = retval
        return retval
    
    ## Default constructor
    #@param base: The basic number from where to start
    #@param extratag: Extra string to be added to the IDs
    def __init__(self,base=0,extratag=''):
        """
        Constructs a helper to handle unique ids
        """
        self.basenum=base
        self.thrnum=base
        self.algParnum=base
        self.dqParamnum=base
        self.dqRefnum=base
        self.dqCondnum=base
        self.thrTag+=extratag+'-'
        self.algParTag+=extratag+'-'
        self.dqParamTag+=extratag+'-'
        self.dqRefTag+=extratag+'-'
        self.dqRegTag+=extratag+'-'
        self.dqCondTag+=extratag+'-'
        self.idx= { "DQThreshold" : { self.thrTag : base } ,
                    "DQAlgorithmParameter" : {self.algParTag : base } ,
                    "DQParameter" : {self.dqParamTag : base } ,
                    "DQReference" : {self.dqRefTag : base} ,
                    "DQRegion"    : {self.dqRegTag : base} ,
                    "DQCondition"   : {self.dqCondTag : base} }

    ##  @return: the ID associated with object of type 'type'. If not found returns string 'NULL' 
    #@param type: the object type name  
    def ID(self,type):
        """
        Returns a unique ID associate to the object of type type
        """
        if type in self.idx:
            elem=self.idx[type]
            tag=list(elem.keys())[0]
            elem[tag]+=1
            tag+=repr(elem[tag])
            return tag
        else:
            return 'NULL'
    ## @return: a valid ID for the DQThreshold object type
    def DQThresholdID(self):
        return self.ID('DQThreshold')
    ##@return: a valid ID for the DQAlgorithmParameter object type
    def DQAlgorithmParameterID(self):
        return self.ID('DQAlgorithmParameter')
    ##@return: a valid ID for the DQParameter object type
    def DQParameterID(self):
        return self.ID('DQParameter')
    ##@return: a valid ID for the DQReference object type
    def DQRefernceID(self):
        return self.ID('DQReference')
    ##@return: a valid ID for the DQCondition object type
    def DQConditionID(self):
        return self.ID('DQCondition')
    ##@return: a valid ID for the DQRegion object type
    def DQRegionID(self):
        return self.ID('DQRegion')

##Base class for exceptions
class BaseException(Exception):
    """
    Base exception for the package
    """
    def __init__(self,msg):
        self.msg = msg
    def __str__(self):
        return 'ERROR: '+str(self.msg)

## Creates a pair of threshold objects
#@param name: name of parameter the limits apply to
#@param warning: limit for good/warning (green/yellow)  
#@param error: limit for warning/error (yellow/red)
#@param id: base ID for these objects
#@return: a dictionary with two keys, 'green' and 'red', which correspond to the two thresholds   
def make_thresholds(name, warning, error, id=IDHelper.getInstance()):
    """
    Creates a pair of DQThresholds with name and two values for warning and error limits.
    In online implementation of DQMF Warning is known as "green threshold" while error is known as "red threshold"
    """
    threshpair = {}
    if id.__class__.__name__ == 'IDHelper':
        idpair = [id, id]
    else:
        idpair = ['green_'+id, 'red_'+id]

    from . import DQElements
    threshpair['green'] = DQElements.DQThreshold(id=idpair[0], name=name, value=warning)
    threshpair['red'] = DQElements.DQThreshold(id=idpair[1], name=name, value=error)
    return threshpair

